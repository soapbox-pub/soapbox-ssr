# Soapbox SSR (Server-Side Rendering)

> :warning: This project is experimental and not production ready.

An Elixir microservice to enable SSR with Soapbox against any backend.

Behind the scenes it performs API requests to the backend and serves the site's `index.html` with injected metadata.

## Up and running

  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:3039`](http://localhost:3039) from your browser.

## Configuration

The following environment variables are supported.

- `SSR_BACKEND_URL` - URL to the backend, for example: `https://gleasonator.com`. (Required)
- `SSR_SOAPBOX_DIR` - The installation directory of Soapbox FE. The `index.html` should be at the root of this directory. (Default: `/opt/soapbox-fe/static`)
- `SSR_API_CACHE_TTL` - Time in milliseconds to cache public API responses. (Default: `5000`, 5 seconds)

## License

Soapbox SSR is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Soapbox SSR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Soapbox SSR.  If not, see <https://www.gnu.org/licenses/>.
