defmodule SoapboxSSRWeb.SoapboxController do
  @moduledoc """
  One controller to rule them all!

  All routes go through this controller, but they have a shared
  set of params which get passed to the injector.
  """
  use SoapboxSSRWeb, :controller
  alias SoapboxSSR.Injector

  @doc "Serve the index.html with injected metadata depending on the params."
  @spec index(conn :: Plug.Conn.t(), params :: map()) :: Plug.Conn.t()
  def index(conn, params) do
    {:ok, body} = File.read(index_file_path())

    response = Injector.inject(body, params)

    conn
    |> put_resp_content_type("text/html")
    |> send_resp(200, response)
  end

  # Path to the index.html from config.
  @spec index_file_path() :: String.t()
  defp index_file_path() do
    Path.join([Application.get_env(:soapbox_ssr, :soapbox_dir), "index.html"])
  end
end
