defmodule SoapboxSSRWeb.Router do
  use SoapboxSSRWeb, :router

  # Route everything to one controller.
  scope "/", SoapboxSSRWeb do
    match :*, "/@:acct/posts/:status_id", SoapboxController, :index
    match :*, "/@:acct/:status_id", SoapboxController, :index
    match :*, "/@:acct", SoapboxController, :index
    match :*, "/users/:acct/statuses/:status_id", SoapboxController, :index
    match :*, "/users/:acct", SoapboxController, :index
    match :*, "/statuses/:status_id", SoapboxController, :index
    match :*, "/notice/:status_id", SoapboxController, :index
    match :*, "/*path", SoapboxController, :index
  end
end
