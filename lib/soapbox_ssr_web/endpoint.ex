defmodule SoapboxSSRWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :soapbox_ssr

  # Serve Soapbox static files.
  plug Plug.Static,
    at: "/",
    from: {Application, :get_env, [:soapbox_ssr, :soapbox_dir]},
    gzip: false,
    only: ~w(packs instance sw.js report.html)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head
  plug SoapboxSSRWeb.Router
end
