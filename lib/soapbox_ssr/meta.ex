defmodule SoapboxSSR.Meta do
  @moduledoc """
  Fetch metadata from the backend and parse it into HTML.
  """
  alias SoapboxSSR.BackendClient
  import Phoenix.HTML.Tag, only: [tag: 2, content_tag: 2]

  defstruct title: "", description: "", image: ""

  @type t :: %__MODULE__{
          title: String.t(),
          description: String.t(),
          image: String.t()
        }

  @spec get(params :: map()) :: String.t()
  def get(params) do
    params
    |> build()
    |> meta_to_html()
  end

  # With a status
  @spec build(params :: map()) :: __MODULE__.t() | Tesla.Env.result()
  def build(%{"status_id" => status_id}) do
    with {:ok, %{status: 200, body: %{"account" => %{} = account} = status}} <-
           BackendClient.fetch_status(status_id) do
      %__MODULE__{
        title: "#{account["display_name"]} (@#{account["acct"]})",
        description: status["content"],
        image: account["avatar_static"]
      }
    end
  end

  # With an account
  def build(%{"acct" => acct}) do
    with {:ok, %{status: 200, body: %{} = account}} <- BackendClient.account_lookup(acct) do
      %__MODULE__{
        title: "#{account["display_name"]} (@#{account["acct"]})",
        description: account["note"],
        image: account["avatar_static"]
      }
    end
  end

  # Fallback
  def build(_) do
    with {:ok, %{status: 200, body: %{} = instance}} <- BackendClient.fetch_instance() do
      %__MODULE__{
        title: instance["title"],
        description: instance["description"],
        image: instance["thumbnail"]
      }
    end
  end

  @doc "Parse a %Meta{} struct into an HTML string with tags for Open Graph and Twitter."
  @spec meta_to_html(meta :: __MODULE__.t()) :: String.t()
  def meta_to_html(%__MODULE__{} = meta) do
    [
      content_tag(:title, meta.title),
      tag(:meta, name: "description", content: meta.description),
      # Open Graph
      tag(:meta, name: "og:title", content: meta.title),
      tag(:meta, name: "og:description", content: meta.description),
      tag(:meta, name: "og:image", content: meta.image),
      # Twitter
      tag(:meta, name: "twitter:title", content: meta.title),
      tag(:meta, name: "twitter:description", content: meta.description),
      tag(:meta, name: "twitter:image", content: meta.image),
      tag(:meta, name: "twitter:card", content: "summary")
    ]
    |> Phoenix.HTML.html_escape()
    |> Phoenix.HTML.safe_to_string()
  end

  @spec meta_to_html(any()) :: String.t()
  def meta_to_html(_), do: ""
end
