defmodule SoapboxSSR.Injector do
  @moduledoc """
  Given an HTML input and params, generate the page metadata
  and inject it into the HTML.

  This behavior mirrors Pleroma, and performs a find-replace
  on this HTML comment (also the same as Pleroma):

  ```html
  <!--server-generated-meta-->
  ```
  """
  @placeholder "<!--server-generated-meta-->"

  @doc "Get metadata for the params and inject it into the given HTML."
  @spec inject(body :: String.t(), params :: map()) :: String.t()
  def inject(body, params) do
    meta_content = SoapboxSSR.Meta.get(params)
    String.replace(body, @placeholder, meta_content)
  end
end
