defmodule SoapboxSSR.BackendClient do
  @moduledoc """
  Tesla-based HTTP client which makes API requests to the backend.
  Responses from the API are cached for 5 seconds by default.
  """

  @doc "Fetch instance data from the backend."
  @spec fetch_instance() :: Tesla.Env.result()
  def fetch_instance() do
    Tesla.get(client(), "/api/v1/instance")
  end

  @doc "Fetch a status by ID from the backend."
  @spec fetch_status(status_id :: String.t()) :: Tesla.Env.result()
  def fetch_status(status_id) do
    Tesla.get(client(), "/api/v1/statuses/#{URI.encode(status_id)}")
  end

  @doc "Fetch an account by its FQN from the backend."
  @spec account_lookup(acct :: String.t()) :: Tesla.Env.result()
  def account_lookup(acct) do
    Tesla.get(client(), "/api/v1/accounts/lookup", query: [acct: acct])
  end

  @doc """
  Build a custom Tesla client from runtime configuration.
  It expects the following configuration:

  ```elixir
  config :soapbox_ssr,
    backend_url: "https://gleasonator.com",
    api_cache_ttl: 5000
  ```
  """
  @spec client() :: Tesla.Client.t()
  def client() do
    backend_url = Application.get_env(:soapbox_ssr, :backend_url)
    api_cache_ttl = Application.get_env(:soapbox_ssr, :api_cache_ttl)

    middleware = [
      {Tesla.Middleware.BaseUrl, backend_url},
      {Tesla.Middleware.Cache, ttl: api_cache_ttl},
      Tesla.Middleware.JSON
    ]

    Tesla.client(middleware)
  end
end
