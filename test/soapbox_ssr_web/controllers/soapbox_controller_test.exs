defmodule SoapboxSSRWeb.SoapboxControllerTest do
  use SoapboxSSRWeb.ConnCase, async: true

  setup do
    Tesla.Mock.mock(&SoapboxSSR.Mock.common/1)
    :ok
  end

  test "GET / injects the instance data", %{conn: conn} do
    result = get(conn, "/")
    assert result.resp_body =~ "Gleasonator"
    assert result.resp_body =~ "Building the next generation of the Fediverse."
  end

  test "GET /@:acct injects account data", %{conn: conn} do
    result = get(conn, "/@alex")
    assert result.resp_body =~ "Alex Gleason (@alex)"
    assert result.resp_body =~ "I&amp;#39;m vegan btw"
  end

  test "GET /@:acct/posts/:status_id injects status data", %{conn: conn} do
    result = get(conn, "/@alex/posts/103874034847713213")
    assert result.resp_body =~ "Alex Gleason (@alex)"
    assert result.resp_body =~ "What is tolerance?"
  end

  test "GET / renders index.html for API error", %{conn: conn} do
    result = get(conn, "/@alex/posts/ERROR")
    assert result.resp_body =~ "<!DOCTYPE html>"
    refute result.resp_body =~ "<!--server-generated-meta-->"
    refute result.resp_body =~ "og:description"
  end

  test "GET /@:acct renders index.html for API error", %{conn: conn} do
    result = get(conn, "/@ERROR")
    assert result.resp_body =~ "<!DOCTYPE html>"
    refute result.resp_body =~ "<!--server-generated-meta-->"
    refute result.resp_body =~ "og:description"
  end

  test "GET /@:acct/posts/:status_id renders index.html for API error", %{conn: conn} do
    result = get(conn, "/@alex/posts/ERROR")
    assert result.resp_body =~ "<!DOCTYPE html>"
    refute result.resp_body =~ "<!--server-generated-meta-->"
    refute result.resp_body =~ "og:description"
  end
end
