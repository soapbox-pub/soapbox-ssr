defmodule SoapboxSSR.BackendClientTest do
  use SoapboxSSRWeb.ConnCase, async: true
  alias SoapboxSSR.BackendClient

  setup do
    Tesla.Mock.mock(&SoapboxSSR.Mock.common/1)
    :ok
  end

  test "fetch_instance/0" do
    {:ok, %{body: %{"title" => "Gleasonator"}}} = BackendClient.fetch_instance()
  end

  test "fetch_status/1" do
    {:ok, %{body: %{"account" => %{"acct" => "alex"}, "content" => "<p>What is tolerance?</p>"}}} =
      BackendClient.fetch_status("103874034847713213")
  end

  test "account_lookup/1" do
    {:ok, %{body: %{"display_name" => "Alex Gleason"}}} = BackendClient.account_lookup("alex")
  end

  test "client/0" do
    %Tesla.Client{} = BackendClient.client()
  end
end
