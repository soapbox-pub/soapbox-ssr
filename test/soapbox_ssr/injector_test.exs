defmodule SoapboxSSR.InjectorTest do
  use SoapboxSSRWeb.ConnCase, async: true
  alias SoapboxSSR.Injector

  setup do
    Tesla.Mock.mock(&SoapboxSSR.Mock.common/1)
    :ok
  end

  test "inject/2 with status data" do
    params = %{"acct" => "alex", "status_id" => "103874034847713213"}
    result = Injector.inject(index_html(), params)
    assert result =~ "Alex Gleason (@alex)"
    assert result =~ "What is tolerance?"
  end

  test "inject/2 with account data" do
    params = %{"acct" => "alex"}
    result = Injector.inject(index_html(), params)
    assert result =~ "Alex Gleason (@alex)"
    assert result =~ "I&amp;#39;m vegan btw"
  end

  test "inject/2 with no params falls back to instance data" do
    result = Injector.inject(index_html(), %{})
    assert result =~ "Gleasonator"
    assert result =~ "Building the next generation of the Fediverse."
  end

  defp index_html(), do: File.read!("test/fixtures/static/index.html")
end
