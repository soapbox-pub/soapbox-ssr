defmodule SoapboxSSR.MetaTest do
  use SoapboxSSRWeb.ConnCase, async: true
  alias SoapboxSSR.Meta

  @instance_thumbnail "https://media.gleasonator.com/c0d38bde6ef0b3baa483f574797662ebd83ef9e1a1162e8e4fcd930bb4b3c068.png"
  @alex_avatar "https://media.gleasonator.com/876a0d19821a0a2fea30841c689ce9166a1ca1521dd06fb6ba27b6eab8a99fcd.png"

  setup do
    Tesla.Mock.mock(&SoapboxSSR.Mock.common/1)
    :ok
  end

  test "build/1 with status data" do
    result = Meta.build(%{"acct" => "alex", "status_id" => "103874034847713213"})
    assert result.title == "Alex Gleason (@alex)"
    assert result.description =~ "What is tolerance?"
    assert result.image == @alex_avatar
  end

  test "build/1 with account data" do
    result = Meta.build(%{"acct" => "alex"})
    assert result.title == "Alex Gleason (@alex)"
    assert result.description =~ "I&#39;m vegan btw"
    assert result.image == @alex_avatar
  end

  test "build/1 with no params falls back to instance data" do
    result = Meta.build(%{})
    assert result.title == "Gleasonator"
    assert result.description =~ "Building the next generation of the Fediverse."
    assert result.image == @instance_thumbnail
  end

  test "get/1 with status data" do
    result = Meta.get(%{"acct" => "alex", "status_id" => "103874034847713213"})
    assert result =~ "Alex Gleason (@alex)"
    assert result =~ "What is tolerance?"
    assert result =~ @alex_avatar
  end

  test "get/1 with account data" do
    result = Meta.get(%{"acct" => "alex"})
    assert result =~ "Alex Gleason (@alex)"
    # FIXME: it sanitized an already sanitized string
    assert result =~ "I&amp;#39;m vegan btw"
    assert result =~ @alex_avatar
  end

  test "get/1 with no params falls back to instance data" do
    result = Meta.get(%{})
    assert result =~ "Gleasonator"
    assert result =~ "Building the next generation of the Fediverse."
    assert result =~ @instance_thumbnail
  end
end
