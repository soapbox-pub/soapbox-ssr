defmodule SoapboxSSR.Mock do
  @moduledoc "Tesla HTTP mocks."

  @doc "Common mocks."
  def common(%{method: :get, url: "https://backend.tld/api/v1/instance"}) do
    %Tesla.Env{status: 200, body: fixture("instance.json")}
  end

  def common(%{
        method: :get,
        url: "https://backend.tld/api/v1/accounts/lookup",
        query: [acct: "alex"]
      }) do
    %Tesla.Env{status: 200, body: fixture("account.json")}
  end

  def common(%{method: :get, url: "https://backend.tld/api/v1/statuses/103874034847713213"}) do
    %Tesla.Env{status: 200, body: fixture("status.json")}
  end

  def common(%{
        method: :get,
        url: "https://backend.tld/api/v1/accounts/lookup",
        query: [acct: "ERROR"]
      }) do
    %Tesla.Env{status: 500, body: ""}
  end

  def common(%{url: "https://backend.tld/api/v1/statuses/ERROR"}) do
    %Tesla.Env{status: 500, body: ""}
  end

  defp fixture(filename) do
    ["test/fixtures", filename]
    |> Path.join()
    |> File.read!()
    |> Jason.decode!()
  end
end
