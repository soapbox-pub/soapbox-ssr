# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :soapbox_ssr,
  namespace: SoapboxSSR,
  soapbox_dir: "/opt/soapbox-fe/static",
  api_cache_ttl: 5000

# Configures the endpoint
config :soapbox_ssr, SoapboxSSRWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: SoapboxSSRWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: SoapboxSSR.PubSub,
  live_view: [signing_salt: "Whb0e5ET"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Use Hackney for Tesla HTTP
config :tesla, adapter: Tesla.Adapter.Hackney

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"

# Import environment specific secrets file.
if File.exists?("./config/#{config_env()}.secret.exs") do
  import_config "#{config_env()}.secret.exs"
end
