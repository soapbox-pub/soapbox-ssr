import Config

config :soapbox_ssr,
  backend_url: "https://backend.tld",
  soapbox_dir: Path.join([File.cwd!(), "test/fixtures/static"]),
  api_cache_ttl: 5000

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :soapbox_ssr, SoapboxSSRWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "Sir/M7A5EDtAN6mRAaWgRHfTWG9J83/N0azBEZoZWw0zcBP6clm1iVsvi/72VlLE",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

# Tesla Mock HTTP client
config :tesla, adapter: Tesla.Mock
